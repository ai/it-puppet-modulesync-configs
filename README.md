# CERN Puppet Module Sync Configuration

This repository contains templated files that can be synced
to all puppet modules and hostgroups. The
[modulesync](https://github.com/voxpupuli/modulesync) utility is
used for this purpose.

To enable your puppet module to be updated please
add it to the file [managed_modules.yml](managed_modules.yml) with a merge request.

Once a module is enabled a new branch called *modulesync*
will be created by an overnight Rundeck job.
This will be a branch from *qa* with synced files applied
on top as a commit. If no changes need to be applied because *qa* is
already in sync with these templates no branch will be created.

## Merging modulesync branch
The same Rundeck job that creates the *modulesync* branch, it also creates a merge
request to *qa*. If you want to force the job to run, the next command can be used:
```
$ ai-modulesync -r it-puppet-[hostgroup|module]-{your_repository_name}
```
As an example, the next command would run msync and create a merge request from
*modulesync* to *qa* for punch hostgroup:
```
$ ai-modulesync -r it-puppet-hostgroup-punch
```
Once the branch *modulesync* is merged, you can safely delete it from your repository.

## Which files are and are not synced.
The full collection of files to be synced are located
in [moduleroot](moduleroot). In particular the following files are
maintained.

* `README.md`: top level README.md file.
* `Dockerfile`: top level Dockerfile to run tests locally
* `.gitlab-ci.yml`: contains test command matrix.
* `.msync.yml`: contains version of modulesync templates.
* `.gitignore`: ignore some files, especially ones created during testing.
* `ci/Rakefile`: rake tasks to do testing.
* `ci/Gemfiles`: lists ruby gems to do testing.
* `ci/boilerplate`: collection of examples files that can be copied into code 
   directory
* `ci/.bundle/config` configuration for the bundle command
* `code/spec/spec/support/spec/mocked_functions_helper.rb.erb` Helpers for mocking functions, not synced by default.

The files inside `code` and `data` will never be touched by modulesync
by default however certain files can be using  `unmanaged: false`
option as below.

## A note on the boilerplate

`ci/boilerplate` contains a collection of files that you may or may
not copy into `code/`. Upstream modules typically come with everything
that's needed to run tests within this framework, however CERN modules
and hostgroups might profit from the boilerplate to get things
started.

Please note that files in `ci/boilerplate` are just examples and once
copied over it's your responsibility to maintain them. For instance,
the list of supported operating systems declared in
[`code/metadata.json`](https://puppet.com/docs/puppet/7/modules_metadata.html)
is something that you'll likely have to modify at some point. If in
doubt, take a look at the latest example provided by the boilerplate.

## Files conditionally copied into code area at gitlab CI runtime.
Certain files are copied directly from the `ci/boilerplate` directory
to the `code` directory when the CI tests run within gitlab. This
happens only if the destination file is not already present within
the code directory For instance `ci/boilerplate/.rubocop.yml` is
copied to `code/.rubocop.yml` if the file does not exist. This allows
the rubocop checks to run sensibly.

# Test information
The following list shows the list of tests executed:

* puppet lint tests.  Rake target *lint*.
* puppet validate tests. Rake target *validate*.
* puppet rspec tests. Rake target *spec*.

Acceptance tests are not currently executed but there is a work in progress to
add them to the list before.

Please take into account that thanks to
[`rspec-puppet-facts`](https://github.com/mcanevet/rspec-puppet-facts),
if you use the `on_supported_os` construction in your tests, [the rspec
test cases will be
exercised](https://github.com/mcanevet/rspec-puppet-facts#specifying-the-supported-operating-systems)
on all the supported operating system versions declared in
`metadata.json`.

# Running tests
The following sections show the two ways to run the tests: Locally and by GitLab.

## Running the tests locally
Configure CERN's GitLab to [trust your SSH public key](https://gitlab.cern.ch/profile/keys).

Follow the instructions in `.gitlab-ci.yml` for exact commands of tests though
you may need to drop the `--local` flag if you have not downloaded
the gems once.

The next is a working example for *aiadm8.cern.ch*:

### Install an alternate ruby
It may be necessary to install an alternate ruby to that provided by the operating
system. The `rbenv` command is available on aiadm.

```
rbenv init  # follow the instructions
rbenv install 2.7.8   # Suitable for Puppet 7
rbenv local 2.7.8
```

### Run tests

```bash
git clone https://:@gitlab.cern.ch:8443/ai/it-puppet-module-yourmodule.git
cd code
BUNDLE_GEMFILE=../ci/Gemfile bundle install
BUNDLE_GEMFILE=../ci/Gemfile bundle exec rake --rakefile ../ci/Rakefile test
```

You can switch from the recommended version of Puppet to a particular
version of Puppet by issuing bundle update. There's no need to install
anything from scratch. For instance, to jump to the latest Puppet 6:

```bash
BUNDLE_GEMFILE=../ci/Gemfile PUPPET_VERSION='~> 6.0' bundle update
BUNDLE_GEMFILE=../ci/Gemfile PUPPET_VERSION='~> 6.0' bundle exec rake --rakefile ../ci/Rakefile test
```

To jump again to the recommended version:

```bash
BUNDLE_GEMFILE=../ci/Gemfile bundle update
BUNDLE_GEMFILE=../ci/Gemfile bundle exec rake --rakefile ../ci/Rakefile test
```

In time the commands above will be simplified.

It's recommended though to install the bundle and run the tests on a local
file system to have faster runs. `aiadm8-homeless.cern.ch` is a good option for
this.

## Mocking functions that require CERN services

Functions for connecting to PuppetDB or Roger sometimes require mocking
in puppet rspec tests. To help with this an rspec helper is available.

Enable its deployment via modulesync in the `~/.sync.yml` file:

```yaml
code/spec/support/spec/mocked_functions_helper.rb:
  unmanaged: false
```

Once deployed read the header of file for details on how to use
these mocked functions for `query_facts`, `puppetdb_query`, ...

## Running the tests locally with Dockerfile
See the comments in the  Dockefile at top level for details on using this method.

## Generating REFERENCE.md from puppet strings
If modules contain puppet strings then a REFERENCE.md can be generated locally
```bash
BUNDLE_GEMFILE=../ci/Gemfile PUPPET_VERSION='~> 6.15.0' bundle exec rake --rakefile ../ci/Rakefile strings:generate:reference
```
This will generate a REFERENCE.md that can be added to your repository.

## Running the tests on GitLab
Pipelines running your tests will only start if GitLab CI is enabled on the
repository where you're running your tests. The
[GitLab documentation](https://docs.gitlab.com/ce/ci/enable_or_disable_ci.html)
describes what flag has to be activated to enable it.

# Configuring your repository

The next section shows useful information on how to configure your repository.

## Merge Request configuration
It is recommended to configure your repository's merge request to use
`Merge commit with semi-linear history` or `Fast-forward merge`. The
[GitLab documentation](https://docs.gitlab.com/ee/user/project/merge_requests/fast_forward_merge.html)
Without this a merge request passing tests may result in failed tests after merge.

## Adjusting or Disabling Particular Tests
The *modulesync* utility has the ability to be influenced by
a control file within your own repository. Create a file `.sync.yml`
in your repository at the top level.

The example below will:

* Disable the rake target that runs spec tests.
* Disable a particular lint test.
* Set extra variables that are set in top scope. e.g. `$::rootusers` as okay.
  Variables set in foreman (ENC) are accessed as `$::rootusers` in puppet
  manifests. This is exactly what the linter [top_scope_facts](https://github.com/mmckinst/puppet-lint-top_scope_facts-check)
  looks for so they must be explicitly ignored from this lint test.
* Exclude an extra directory from being processed for lint
  or compile validation.
* Configure `strict_variables` environment to rspec-puppet.
* Display custom set of branch build status badges in README.md.
* Make the rubocop test critical
* Make the test using the very latest version critical
* Mark additional content for inclusion `.gitlab-ci.yml`. 
  [gitlab include docs](https://docs.gitlab.com/ee/ci/yaml/#include)
* The files `code/.rubocop.yml`, `code/spec/spec_helper.rb`, `code/.rspec`,
  `code/spec/support/spec/mocked_functions_helper.rb.erb` and `code/.rspec_parallel`
  will be maintained centrally from templates.
* Use the deprecated mocha for mocking.
* Disable the rubocop check Naming/VariableNumber
* Install an extra gem (webmock) for tests during (localonly) CI.

```yaml
---
code/spec/spec_helper.rb:
  mock_with: ':mocha'

ci/Rakefile:
  extra_disabled_rake_targets:
    - spec
  extra_top_scope_variables:
    - 'rootegroups'
  extra_disabled_lint_checks:
    - 'disable_quoted_booleans'
    - 'disable_parameter_type'
    - 'disable_parameter_documentation'
    - 'disable_top_scope_facts'
  extra_exclude_paths:
    - examples/**/*

.gitlab-ci.yml:
   strict_variables: 'no'
   rubocop_allow_failure: false
   puppet_latest_allow_failure: false
   localonly: false
   include:
     - local: /.gitlab-ci-local.yml
     - project: 'my-group/my-project
       file: /.gitlab-ci-another-project.yml

ci/Gemfile:
  optional:
    ':test':
      - gem: webmock

README.md:
  branches:
    - master
    - qa
    - modulesync
    - testing

code/spec/spec_helper.rb:
  unmanaged: false

code/spec/support/spec/mocked_functions_helper.rb:
  unmanaged: false

code/.rubocop.yml:
  unmanaged: false
  extra:
    Naming/VariableNumber:
      Enabled: False

code/.rspec:
  unmanaged: false

code/.rspec_parallel:
  unmanaged: false

```

Once a `.sync.yml` file is present the next run of modulesync will create
the modulesync branch with these updates which can then be merged in the normal
way.

For full details of what can be set
read the [source](moduleroot) and [default configuration](config_defaults.yml) file.

## Releasing These Templates
These templates are are now versioned. Docker images generated and used by subsequent tests are now versioned with the same version number.

To create and tag a release from a release candidate:

0. Prepare a bundle `bundle install` to get the modulesync command msync.
1. Verify `ci_images` repository builds okay running `bundle exec msync update -f it-puppet-module-ci_images --noop`.
2. Edit `config_defaults.yml` and remove `-rc0` from the current version number (e.g. `X.Y.Z-rc0` to `X.Y.Z`). Follow [Semantic Versioning specificatoin](http://semver.org/).
3. Update the `CHANGELOG.md` for the new release, adding the new changes for this version.
4. Commit and push the change, creating also a new tag for the new version.

    ```bash
    $ git add config_defaults.yml CHANGELOG.md
    $ git commit -m 'Prepare release X.Y.Z'
    $ git push
    $ git tag X.Y.Z
    $ git push --tags
    ```
5. Run `ai-modulesync -r it-puppet-module-ci_images` to update the repository with the last changes. A merge request will be created in the repository and requires to be accepted manually.
6. Merge modulesync merge requests on ci\_images module repository.
7. Update `config_defaults.yml` and make a new rc0 (e.g. Move version `X.Y.Z` to `X.Y.Z+1-rc0`).
8. Commit and push this change.

    ```bash
    $ git add config_defaults.yml
    $ git commit -m 'Prepare I.J.K+1-rc0'
    $ git push
    ```
9. Test that everything is correct running `bundle exec msync update -f it-puppet-module-ci_images --noop`.

10. Finally apply the rc templates to the `ci_images`. The previous step will have created a git repository 
    in `modules/ai/it-puppet-module-ci_images`. Commit the changes in there and push. This will create a new `rc` CI image 
    and the production image from tag cannot be ovewritten.  

    ```bash
    cd modules/ai/it-puppet-module-ci_images
    git diff
    git add .
    git commit -m 'Release candiate X.Y.Z+1-rc0'
    git push
    ```
