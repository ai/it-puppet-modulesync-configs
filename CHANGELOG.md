## ????-??-?? Release 2.11.2-rc0

## 2025-02-14 Release 2.11.1

- Re-release for release process error

## 2025-02-14 Release 2.11.0

- Optional rspec helper to mock CERN functions, e.g puppetdb_query
- Puppet 8 tests using latest testing gems, in particular newer factsdb without legacy facts. Note it is unclear today if legacy facts will be disabled for Puppet 8 but doing so in rspec makes sense.
- A `.gitattributes` file is now deployed to all.
- Remove legacy facts for example boilerplate.

## 2025-01-14 Release 2.10.1
- Avoid use of YAML.load_stream in CI as not safe.
- .gitignore ignore .vscode directory.
- .gitignore ignore MacOS special files.
- Remove .el7 from example metadata.
- Support installing extra gems per ai project, e.g webmock.
- Install `augparse` in image.
- Check for incorrectly named rspec files.

## 2024-05-23 Release 2.9.2
- Add rubocop-performance gem
- Test also Puppetlabs' concat module

## 2024-02-19 - Release 2.9.1
- Check for wrongly named spec files. @iven
- Generate junit file per thread, fixes gitlab parsing. @pigueira
- Correct OSes in example metadata.yml file.
- Put STRICT_VARIABLES (if set) in correct place.

## 2024-02-01 - Release 2.8.1
- Multiplex the SSH connections to gitlab.

## 2023-11-29 - Release 2.8.0
- Addition of Puppet 8 tests - these are marked allow_failure for all
- Stop installing ruby-ldap gem - will not be available in P8.
- Bug - Remove erroneous spaces when customising rubocop configuration
- Docs - Correct rubocop extension example
- Correct filename of default facts in boilerplate example
- Add a new empty gitlab stage posttests for injecting extra tests
- Check YAML files for case where they contain >1 document (double ---)

## 2023-10-02 - Release 2.7.2
- Do not request docker tagged runners

## 2023-08-22 - Release 2.7.1
- Bug fix rbenv 2.7.8 definition required

## 2023-08-22 - Release 2.7.0
- Use Kaniko for image creation
- Drop puppet7_latest and use Puppet 7.24 for puppet7

## 2023-07-10 - Release 2.6.4
- Set LC_ALL for puppet manifests containing Unicode

## 2023-07-10 - Release 2.6.3
- Set LC_ALL for puppet manifests containing Unicode

## 2023-07-03 - Release 2.6.2
- Allow data/<module_name>.yaml in modules

## 2023-06-26 - Release 2.6.1
- pin concurrent-ruby while puppet 7.12 is in play

## 2023-06-26 - Release 2.6.0
- Use alma9 rather than stream8 image for all tests
- Update of all gems
- Tests now using `rbenv` installed ruby. Using system rubies is
  no longer possible. Once puppet8 is eventually introduced really no option.
- On the puppet_latest test only STRICT_VARIABLES=false is no longer an option.
  This feature that allows undefined variables is required little if at all these days.
- Extra YAML checks for hiera data:
   * invalid `.yml` files (should be `.yaml`)
   * warn if <FQDN>.yaml files do not exist in DNS.

## 2022-11-18 - Release 2.5.1
- Pin some gems to work with ruby2.6 on CentOS8
- Avoid confusing "WARNING: code/rspec.xml: no matching files" message
  when no spec tests are present.

## 2022-06-07 - Release 2.4.2
- Pin version rubygem-ast to work with ruby 2.5
- Pin version rubygem-rspec to work with ruby 2.5

## 2022-06-07 - Release 2.4.0
+ Pin version of parallel_tests to allow default ruby 2.5 to continue on C8.
+ First version of Dockerfile to run tests locally, see Dockerfile at top level 
  for details.
+ Extra lines can now be added to `.rubocop.yml` via `.sync.yml`.
+ The default mocking method can be overidden to `:mocha` in particular 
  via `.sync.yml`.
+ Assuming the default mocking method is left in place the systemd process
  is now mocked so the systemd service provider is always chosen.
+ It may be necessary to rename `code/spec/default_facts.yml` to
  `code/spec/default_module_facts.yml` if not done so already.

## 2022-02-25 - Release 2.3.0
+ Realigns to same rubygems as VoxPupuli and Puppetlabs.
  This includes a significant jump in rubocop to 1.22.3 which
  will likely result in quite a few ruby style fixes
  needing to be applied.
  In addition there are three new linters enabled.
  * https://github.com/iasha102/puppet-lint-parameter_type-check
      * Verifies that all parameters are typed
  * https://github.com/voxpupuli/puppet-lint-param-docs
      * Verifies that all parameters are documented or manifest is marked as private api.
  * https://github.com/mmckinst/puppet-lint-top_scope_facts-check
      * Verifies that `$::myfact` is not being used anymore.
      * Foreman defined variables are an exception and may need to marked as exceptions
        in `.sync.yml`. All the obvious foreman variables have been added already.
  As always any linter can be disabled via the `.sync.yml` file.
  Documentation for `.sync.yml` is available in the [README.md](README.md)
+ Use CentOS 8 Stream images rather than CentOS 8

## 2022-01-25 - Release 2.2.0
+ Drop Puppet6 and Facter 3 tests.

## 2022-01-06 - Release 2.1.1
+ Simple rebuild to get latest gems, in particular facterdb supporting C/R9.

## 2021-10-05 - Release 2.1.0
+ Puppet 7.11.0 (facter 4) now considered critical always.
+ Latest facterdb includes CentOS Stream/RHEL 9 facts.
+ Puppet 6 test updated to 6.20.0.
+ Document the link between metadata.json and os_supported_os.
+ Document a bit better what the boilerplate is for.
+ Remove SLC6/RH6.
## 2021-02-01 - Release 2.0.0
+ All documentation examples are now C8 (aiadm8.cern.ch) based.
+ Gitlab CI is executed on C8 images.
+ Addition of non-critical Puppet 7 with Facter 4 tests
+ The puppet6\_latest test is now always critical.
+ Rubocop boilerplate example for modules radically simplified.
+ Some files in code **can** now be modulesync managed. See [README.md](README.md)
  for `unmanaged: false` override.
   * `code/.rubocop.yml`
   * `code/.rspec` 
   * `code/.rspec_parallel` 
   * `code/spec/spec_helper.rb`

## 2020-12-04 - Release 1.7.0
+ yaml validate yaml in code/data in addition to data
+ Deploy an `.editorconfig` file per repo https://editorconfig.org/
+ Remove all puppet5 testing
+ Run rubocop with ruby 2.5 rather than 2.4
+ Remove all ruby 2.4 from image
+ Force version 2.0.0 of `abolute_classname_check`.
+ Force facter < 4 which is facter 2, same as puppet-agent 5

## 2020-09-09 - Release 1.6.2
+ Disable manifest\_whitespace\_closing\_bracket\_after test.

## 2020-09-04 - Release 1.6.1
+ Remove puppet5\_latest test.
+ Make `puppet6` (6.15.0) test critical in all cases.
+ Use `voxpupli-test` gem (pulls in all needed gems)
+ Pin version of simplecov so ruby24 works again.
+ New linter tests for white space are installed however they
  have been disabled at CERN for now.

## 2020-04-21 - Release 1.5.0
+ New lint check `cern_legacy_facts` currently looks for obsolete
  `operatingsystemmajorrelease` fact only.
+ Within CI XML reports are now created in addition from rspec tests.
  Gitlab parses these to produce "pretty" output in merge requests.

## 2020-04-01 - Release 1.4.2
+ Bug, correct command separator in Dockerfile.

## 2020-04-01 - Release 1.4.1
+ Install dmidecode and which as needed for facter version 4.

## 2020-03-31 - Release 1.4.0
+ All gems and binstubs are now installed in `~/.aigems`.
  After review it is okay `rm -rf ~/.gems` to reclaim
  the disk space. In addition previously `~/bin` was populated
  with binstubs: `~/bin/puppet`, `~/bin/facter` and many more. These can
  now be deleted. Running `file ~/bin/*` should help you identify them.
+ Simplify gems, re-sync with voxpupuli list
+ New checks
  + `puppet-lint-absolute_classname-check` Now include ::class will fail.
  + `puppet-lint-absolute_classname-check` Now $::foo::bar will fail.
  + `puppet-lint-legacy_facts-check` Now $operatingsystem will fail.
  + `puppet-lint-anchor-check` Now anchor constructs will fail.
+ Remove unused gem groups: `--without system_tests development` no longer required.
+ Fix image URL to pipeline status badge.
+ Example of how to use newer git in README
+ Example `metadata.json` now contains puppet requirement. rspec-puppet will
  load module level hiera data correctly.
+ gems added so that rake strings target works.

## 2020-01-07 - Release 1.3.0
+ Latest facterdb with CentOS 8 facts
+ .gitignore contains `fixtures/{modules|manifests}` and not `fixtures/*`
+ SLC5/C8 removed/added to metadata boilerplate.
+ Install openssh-clients earlier so extra tests also have a token.

## 2019-10-17 - Release 1.2.1
+ Restore wrongly deleted `.gitlab-ci.yml`

## 2019-10-17 - Release 1.2.0
+ New modulesync parameter `include` can be used to specify
  inclusion of extra files (tests) into `.gitlab-ci.yml`

+ New modulesync parameter `use_gitlab_ci_token` can be set to true to
  use the CI_JOB_TOKEN mechanism to authenticate

## 2019-09-29 - Release 1.1.0
* `ci/vendor` now added to `.gitignore`
* New facterdb includes RHEL 8 facts.

## 2019-06-21 - Release 1.0.2
* Pin rubocop version as VoxPupuli do. New rubocop
  was a headache.

## 2019-06-21 - Release 1.0.1
* bug fix

## 2019-06-21 - Release 1.0.0
* [AI-5527] - Enable multithreaded rspec testing
  If the `spec` rake target was disabled for your
  repository you must now disable `parallel_spec` instead.
* [AI-5498] Improve message when an invalid key is found

## 2018-03-21 - Release 0.8.1
* Bug fix - actually deploy  extra yaml-checks.rb

## 2018-03-21 - Release 0.8.0
* Drop Puppet4 target and replace it by our Puppet5.
* Drop absolute class name check, `include ::foo` can now be
  `include foo`.

## 2018-11-13 - Release 0.7.1
+ New parameter `rubocop_allow_failure` can be set to false
  to make rubocop tests critical.
+ New parameter `puppet_latest_allow_failure` can be set
  to false to make latest (currently 6) puppet tests critical
+ The YAML test now checks if /data and /code/data both exist
  and fails if they do. A common eventually confusing error.
+ The unused puppet3 parameter completly removed.
+ The `.rubocop.yml` is now copied from `ci/boilerplate` to `code/`
  during rubocop tests if the destination file does not exist.

## 2018-10-17 - Release 0.6.1
+ Correct search path for rubocop

## 2018-10-17 - Release 0.6.0
+ Drop puppet4_latest test
+ Make puppet5_latest test critical
+ Add puppet6_latest test not critical

## 2018-07-18 - Release 0.5.1
- Bugfix - only hg modules start with hg_

## 2018-07-18 - Release 0.5.0
+ Enable a rubocop test permitted to fail.
+ Update rspec helpers to latest.
+ Use ruby 2.4 with puppet5 tests.
+ Compile check ruby code (validate method)
+ gitignore more than one vim swap file.
+ hiera.yaml is now enabled in spec_helper
+ Set fact path always.

## 2018-01-11 - Release 0.4.4
+ All templates must now have .erb extension.
+ Remove all references to rubygem-voxpupuli.

## 2017-11-06 - Release 0.4.3
+ Refresh image to get latest gems.

## 2017-09-06 - Release 0.4.2
+ Refresh image to get latest gems. In particular 
  puppet-lint will go from 2.2.1 -> 2.3.0

## 2017-07-03 - Release 0.4.1
+ Drop puppet3 build
+ Enable a non critical puppet5 test.
+ Make puppet4_latest test critical.

## 2017-05-30 - Release 0.3.0
+ Our puppet 4 is now 4.9 and not 4.8.
+ yaml files are now checked that they are not empty.

## 2017-03-05 - Release 0.2.0
+ Clean up redundant comments about CI_BUILD_REF_NAME.
+ Add ._afs* files to .gitignore.
+ merge requests now include version in commit message.

## 2017-05-11 - Release 0.0.2
+ Puppet 3 testing now disabled by default. Can be enbled within `.sync.yml`.

```yaml
.gitlab-ci.yml:
  puppet3: true
```

## 2017-03-03 - Release 0.0.1
+ Start using a release version for modulesync templates.  Adds file `.msync.yml` to top level of git repository.
+ Document `ai-modulesync` command
+ Improve text in README.md, fix typos.
+ List of branches to display build status icons for in the README.md is now a configuration available to customise in `.sync.yml` file. Defaults to master, qa and modulesync.
+ rspec boilerplate for hostgroups now reflects the fact that top hostgroup class is actually hg_<hostgroup>.

## 2016-12-07
+ Add simplecov-console, needed by selinux module.
+ Add --full-index to bundle install, # of gems related.

## 2016-12-02
+ Add net-ldap to list of gems for DB.

## 2016-11-23
+ Pin beaker to work with ruby 2.
+ Install coveralls gem.
+ Install puppet-lint-variable_contains_upcase

## 2016-10-19
+ yaml-lint tests no longer fail if data directory is simply not 
  present.
+ Docker file now has less layers so is smaller.
+ The .sync.yml file is now syntax tested, it being
  bad on one module could cause all modules to fail
  to sync.
+ Uses new Docker image by default, the mater branch of the 
  ci_images module. The image to use can be customised via
  modulesync.

## 2016-09-27
+ New variable can be set in .sync.yml

```yaml
.gitlab-ci.yml:
  all_allow_failure: true
```
It sets all rake tests to be permitted to fail, useful for when
you want to enaable tests but not enforce them.

## 2016-09-26
+ Generate and use customtised docker image.

## 2016-07-05
+ metadata_lint check failing if LANG=en_US.UTF-8 not set.

## 2016-06-23
+ Updated gem list to work with puppet-lint 2.0.0
+ For now disable puppet-lint-variable_contains_upcase pending
  https://github.com/fiddyspence/puppetlint-variablecase/pull/2

## 2016-03-07
* Add more links to README.md.
* Add a CHANGELOG to the sync configurations.
