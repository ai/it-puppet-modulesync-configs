# Current puppet module build results



<table>
<tr>
  <th>Module or Hostgroup</th>
  <th>Master Brach</th>
  <th>QA Branch</th>
  <th>Modulesync Branch</th>
</tr>
<tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-acronnext'>it-puppet-hostgroup-acronnext</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-acronnext/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-acronnext/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-acronnext/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-acronnext/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-acronnext/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-acronnext/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-acronsvc'>it-puppet-hostgroup-acronsvc</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-acronsvc/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-acronsvc/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-acronsvc/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-acronsvc/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-acronsvc/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-acronsvc/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-afssrv'>it-puppet-hostgroup-afssrv</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-afssrv/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-afssrv/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-afssrv/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-afssrv/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-afssrv/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-afssrv/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aiadm'>it-puppet-hostgroup-aiadm</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aiadm/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aiadm/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aiadm/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aiadm/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aiadm/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aiadm/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aiermis'>it-puppet-hostgroup-aiermis</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aiermis/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aiermis/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aiermis/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aiermis/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aiermis/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aiermis/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ailbd'>it-puppet-hostgroup-ailbd</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ailbd/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ailbd/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ailbd/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ailbd/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ailbd/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ailbd/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aimon'>it-puppet-hostgroup-aimon</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aimon/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aimon/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aimon/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aimon/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aimon/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aimon/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims'>it-puppet-hostgroup-aims</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-appserver'>it-puppet-hostgroup-appserver</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-appserver/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-appserver/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-appserver/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-appserver/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-appserver/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-appserver/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-autodecom'>it-puppet-hostgroup-autodecom</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-autodecom/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-autodecom/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-autodecom/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-autodecom/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-autodecom/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-autodecom/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-autofs'>it-puppet-hostgroup-autofs</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-autofs/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-autofs/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-autofs/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-autofs/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-autofs/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-autofs/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-backup'>it-puppet-hostgroup-backup</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-backup/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-backup/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-backup/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-backup/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-backup/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-backup/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-batchinfra'>it-puppet-hostgroup-batchinfra</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-batchinfra/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-batchinfra/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-batchinfra/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-batchinfra/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-batchinfra/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-batchinfra/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-batchmon'>it-puppet-hostgroup-batchmon</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-batchmon/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-batchmon/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-batchmon/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-batchmon/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-batchmon/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-batchmon/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-bcwg'>it-puppet-hostgroup-bcwg</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-bcwg/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-bcwg/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-bcwg/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-bcwg/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-bcwg/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-bcwg/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-bi'>it-puppet-hostgroup-bi</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-bi/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-bi/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-bi/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-bi/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-bi/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-bi/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castor'>it-puppet-hostgroup-castor</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castor/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castor/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castor/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castor/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castor/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castor/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castortapelog'>it-puppet-hostgroup-castortapelog</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castortapelog/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castortapelog/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castortapelog/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castortapelog/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castortapelog/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castortapelog/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cds'>it-puppet-hostgroup-cds</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cds/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cds/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cds/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cds/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cds/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cds/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph'>it-puppet-hostgroup-ceph</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cfmgr'>it-puppet-hostgroup-cfmgr</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cfmgr/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cfmgr/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cfmgr/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cfmgr/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cfmgr/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cfmgr/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_adm'>it-puppet-hostgroup-cloud_adm</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_adm/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_adm/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_adm/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_adm/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_adm/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_adm/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_automation'>it-puppet-hostgroup-cloud_automation</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_automation/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_automation/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_automation/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_automation/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_automation/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_automation/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_baremetal'>it-puppet-hostgroup-cloud_baremetal</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_baremetal/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_baremetal/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_baremetal/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_baremetal/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_baremetal/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_baremetal/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_benchmark'>it-puppet-hostgroup-cloud_benchmark</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_benchmark/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_benchmark/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_benchmark/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_benchmark/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_benchmark/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_benchmark/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_blockstorage'>it-puppet-hostgroup-cloud_blockstorage</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_blockstorage/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_blockstorage/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_blockstorage/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_blockstorage/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_blockstorage/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_blockstorage/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_compute'>it-puppet-hostgroup-cloud_compute</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_compute/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_compute/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_compute/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_compute/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_compute/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_compute/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_dashboard'>it-puppet-hostgroup-cloud_dashboard</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_dashboard/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_dashboard/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_dashboard/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_dashboard/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_dashboard/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_dashboard/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_ec2'>it-puppet-hostgroup-cloud_ec2</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_ec2/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_ec2/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_ec2/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_ec2/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_ec2/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_ec2/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_fileshare'>it-puppet-hostgroup-cloud_fileshare</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_fileshare/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_fileshare/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_fileshare/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_fileshare/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_fileshare/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_fileshare/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_identity'>it-puppet-hostgroup-cloud_identity</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_identity/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_identity/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_identity/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_identity/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_identity/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_identity/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_image'>it-puppet-hostgroup-cloud_image</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_image/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_image/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_image/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_image/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_image/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_image/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_infra'>it-puppet-hostgroup-cloud_infra</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_infra/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_infra/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_infra/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_infra/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_infra/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_infra/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_monitoring'>it-puppet-hostgroup-cloud_monitoring</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_monitoring/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_monitoring/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_monitoring/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_monitoring/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_monitoring/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_monitoring/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_orchestration'>it-puppet-hostgroup-cloud_orchestration</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_orchestration/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_orchestration/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_orchestration/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_orchestration/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_orchestration/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_orchestration/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_projectmanagement'>it-puppet-hostgroup-cloud_projectmanagement</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_projectmanagement/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_projectmanagement/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_projectmanagement/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_projectmanagement/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_projectmanagement/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_projectmanagement/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_telemetry'>it-puppet-hostgroup-cloud_telemetry</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_telemetry/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_telemetry/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_telemetry/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_telemetry/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_telemetry/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_telemetry/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_workflow'>it-puppet-hostgroup-cloud_workflow</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_workflow/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_workflow/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_workflow/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_workflow/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_workflow/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_workflow/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-codereview'>it-puppet-hostgroup-codereview</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-codereview/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-codereview/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-codereview/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-codereview/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-codereview/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-codereview/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-condor'>it-puppet-hostgroup-condor</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-condor/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-condor/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-condor/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-condor/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-condor/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-condor/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-csc'>it-puppet-hostgroup-csc</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-csc/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-csc/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-csc/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-csc/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-csc/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-csc/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cta'>it-puppet-hostgroup-cta</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cta/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cta/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cta/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cta/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cta/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cta/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cvmfs'>it-puppet-hostgroup-cvmfs</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cvmfs/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cvmfs/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cvmfs/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cvmfs/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cvmfs/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cvmfs/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dashboard'>it-puppet-hostgroup-dashboard</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dashboard/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dashboard/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dashboard/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dashboard/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dashboard/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dashboard/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-database'>it-puppet-hostgroup-database</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-database/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-database/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-database/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-database/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-database/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-database/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dbod'>it-puppet-hostgroup-dbod</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dbod/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dbod/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dbod/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dbod/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dbod/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dbod/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dia'>it-puppet-hostgroup-dia</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dia/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dia/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dia/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dia/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dia/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dia/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-doconverter'>it-puppet-hostgroup-doconverter</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-doconverter/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-doconverter/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-doconverter/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-doconverter/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-doconverter/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-doconverter/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-doctor'>it-puppet-hostgroup-doctor</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-doctor/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-doctor/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-doctor/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-doctor/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-doctor/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-doctor/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dpmtb'>it-puppet-hostgroup-dpmtb</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dpmtb/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dpmtb/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dpmtb/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dpmtb/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dpmtb/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dpmtb/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-eos'>it-puppet-hostgroup-eos</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-eos/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-eos/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-eos/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-eos/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-eos/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-eos/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-eostest'>it-puppet-hostgroup-eostest</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-eostest/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-eostest/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-eostest/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-eostest/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-eostest/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-eostest/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-fence'>it-puppet-hostgroup-fence</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-fence/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-fence/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-fence/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-fence/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-fence/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-fence/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-filer'>it-puppet-hostgroup-filer</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-filer/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-filer/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-filer/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-filer/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-filer/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-filer/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-fts'>it-puppet-hostgroup-fts</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-fts/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-fts/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-fts/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-fts/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-fts/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-fts/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-geant4'>it-puppet-hostgroup-geant4</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-geant4/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-geant4/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-geant4/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-geant4/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-geant4/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-geant4/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-grid_wlmanagement'>it-puppet-hostgroup-grid_wlmanagement</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-grid_wlmanagement/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-grid_wlmanagement/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-grid_wlmanagement/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-grid_wlmanagement/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-grid_wlmanagement/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-grid_wlmanagement/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-hadoop'>it-puppet-hostgroup-hadoop</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-hadoop/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-hadoop/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-hadoop/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-hadoop/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-hadoop/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-hadoop/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-haggis'>it-puppet-hostgroup-haggis</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-haggis/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-haggis/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-haggis/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-haggis/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-haggis/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-haggis/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-hardware_labs'>it-puppet-hostgroup-hardware_labs</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-hardware_labs/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-hardware_labs/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-hardware_labs/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-hardware_labs/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-hardware_labs/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-hardware_labs/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-hepdata'>it-puppet-hostgroup-hepdata</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-hepdata/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-hepdata/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-hepdata/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-hepdata/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-hepdata/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-hepdata/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-inspire'>it-puppet-hostgroup-inspire</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-inspire/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-inspire/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-inspire/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-inspire/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-inspire/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-inspire/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-it_es'>it-puppet-hostgroup-it_es</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-it_es/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-it_es/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-it_es/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-it_es/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-it_es/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-it_es/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-itdcim'>it-puppet-hostgroup-itdcim</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-itdcim/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-itdcim/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-itdcim/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-itdcim/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-itdcim/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-itdcim/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-its'>it-puppet-hostgroup-its</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-its/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-its/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-its/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-its/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-its/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-its/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lcgvoms'>it-puppet-hostgroup-lcgvoms</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lcgvoms/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lcgvoms/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lcgvoms/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lcgvoms/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lcgvoms/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lcgvoms/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lhcbmesos'>it-puppet-hostgroup-lhcbmesos</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lhcbmesos/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lhcbmesos/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lhcbmesos/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lhcbmesos/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lhcbmesos/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lhcbmesos/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-licmon'>it-puppet-hostgroup-licmon</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-licmon/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-licmon/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-licmon/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-licmon/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-licmon/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-licmon/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-linuxsupport'>it-puppet-hostgroup-linuxsupport</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-linuxsupport/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-linuxsupport/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-linuxsupport/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-linuxsupport/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-linuxsupport/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-linuxsupport/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lpwan'>it-puppet-hostgroup-lpwan</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lpwan/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lpwan/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lpwan/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lpwan/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lpwan/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lpwan/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsb'>it-puppet-hostgroup-lsb</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsb/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsb/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsb/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsb/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsb/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsb/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxdistcc'>it-puppet-hostgroup-lxdistcc</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxdistcc/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxdistcc/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxdistcc/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxdistcc/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxdistcc/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxdistcc/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxplus'>it-puppet-hostgroup-lxplus</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxplus/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxplus/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxplus/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxplus/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxplus/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxplus/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxsoft'>it-puppet-hostgroup-lxsoft</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxsoft/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxsoft/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxsoft/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxsoft/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxsoft/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxsoft/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mail'>it-puppet-hostgroup-mail</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mail/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mail/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mail/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mail/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mail/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mail/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-malt_iaa'>it-puppet-hostgroup-malt_iaa</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-malt_iaa/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-malt_iaa/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-malt_iaa/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-malt_iaa/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-malt_iaa/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-malt_iaa/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mig'>it-puppet-hostgroup-mig</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mig/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mig/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mig/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mig/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mig/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mig/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mimir'>it-puppet-hostgroup-mimir</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mimir/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mimir/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mimir/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mimir/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mimir/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mimir/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-monitoring'>it-puppet-hostgroup-monitoring</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-monitoring/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-monitoring/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-monitoring/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-monitoring/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-monitoring/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-monitoring/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mw_readiness'>it-puppet-hostgroup-mw_readiness</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mw_readiness/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mw_readiness/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mw_readiness/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mw_readiness/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mw_readiness/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mw_readiness/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-myproxy'>it-puppet-hostgroup-myproxy</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-myproxy/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-myproxy/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-myproxy/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-myproxy/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-myproxy/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-myproxy/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-na61'>it-puppet-hostgroup-na61</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-na61/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-na61/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-na61/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-na61/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-na61/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-na61/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-network_ci'>it-puppet-hostgroup-network_ci</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-network_ci/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-network_ci/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-network_ci/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-network_ci/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-network_ci/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-network_ci/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-network_monitoring'>it-puppet-hostgroup-network_monitoring</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-network_monitoring/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-network_monitoring/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-network_monitoring/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-network_monitoring/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-network_monitoring/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-network_monitoring/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-network_services'>it-puppet-hostgroup-network_services</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-network_services/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-network_services/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-network_services/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-network_services/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-network_services/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-network_services/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ngauth'>it-puppet-hostgroup-ngauth</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ngauth/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ngauth/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ngauth/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ngauth/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ngauth/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ngauth/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-openlab'>it-puppet-hostgroup-openlab</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-openlab/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-openlab/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-openlab/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-openlab/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-openlab/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-openlab/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ourproxy'>it-puppet-hostgroup-ourproxy</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ourproxy/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ourproxy/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ourproxy/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ourproxy/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ourproxy/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ourproxy/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-paas'>it-puppet-hostgroup-paas</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-paas/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-paas/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-paas/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-paas/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-paas/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-paas/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-perfsonar_cern'>it-puppet-hostgroup-perfsonar_cern</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-perfsonar_cern/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-perfsonar_cern/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-perfsonar_cern/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-perfsonar_cern/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-perfsonar_cern/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-perfsonar_cern/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-playground'>it-puppet-hostgroup-playground</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-playground/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-playground/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-playground/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-playground/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-playground/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-playground/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-procurement'>it-puppet-hostgroup-procurement</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-procurement/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-procurement/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-procurement/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-procurement/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-procurement/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-procurement/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-punch'>it-puppet-hostgroup-punch</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-punch/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-punch/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-punch/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-punch/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-punch/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-punch/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-security'>it-puppet-hostgroup-security</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-security/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-security/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-security/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-security/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-security/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-security/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-servicecosting'>it-puppet-hostgroup-servicecosting</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-servicecosting/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-servicecosting/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-servicecosting/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-servicecosting/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-servicecosting/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-servicecosting/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-spectrum'>it-puppet-hostgroup-spectrum</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-spectrum/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-spectrum/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-spectrum/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-spectrum/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-spectrum/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-spectrum/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapedust'>it-puppet-hostgroup-tapedust</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapedust/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapedust/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapedust/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapedust/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapedust/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapedust/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapeserver'>it-puppet-hostgroup-tapeserver</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapeserver/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapeserver/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapeserver/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapeserver/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapeserver/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapeserver/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-teigi'>it-puppet-hostgroup-teigi</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-teigi/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-teigi/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-teigi/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-teigi/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-teigi/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-teigi/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tone'>it-puppet-hostgroup-tone</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tone/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tone/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tone/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tone/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tone/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tone/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-training'>it-puppet-hostgroup-training</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-training/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-training/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-training/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-training/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-training/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-training/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vccondor'>it-puppet-hostgroup-vccondor</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vccondor/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vccondor/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vccondor/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vccondor/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vccondor/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vccondor/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasami'>it-puppet-hostgroup-voatlasami</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasami/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasami/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasami/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasami/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasami/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasami/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasbuild'>it-puppet-hostgroup-voatlasbuild</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasbuild/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasbuild/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasbuild/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasbuild/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasbuild/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasbuild/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasddm'>it-puppet-hostgroup-voatlasddm</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasddm/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasddm/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasddm/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasddm/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasddm/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasddm/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasdkb'>it-puppet-hostgroup-voatlasdkb</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasdkb/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasdkb/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasdkb/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasdkb/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasdkb/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasdkb/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasmisc'>it-puppet-hostgroup-voatlasmisc</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasmisc/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasmisc/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasmisc/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasmisc/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasmisc/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasmisc/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasrucio'>it-puppet-hostgroup-voatlasrucio</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasrucio/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasrucio/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasrucio/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasrucio/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasrucio/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasrucio/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlastz'>it-puppet-hostgroup-voatlastz</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlastz/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlastz/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlastz/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlastz/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlastz/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlastz/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocms'>it-puppet-hostgroup-vocms</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocms/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocms/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocms/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocms/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocms/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocms/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsdb'>it-puppet-hostgroup-vocmsdb</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsdb/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsdb/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsdb/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsdb/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsdb/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsdb/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsfrontier'>it-puppet-hostgroup-vocmsfrontier</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsfrontier/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsfrontier/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsfrontier/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsfrontier/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsfrontier/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsfrontier/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein'>it-puppet-hostgroup-vocmsglidein</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsicms'>it-puppet-hostgroup-vocmsicms</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsicms/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsicms/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsicms/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsicms/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsicms/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsicms/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmssdt'>it-puppet-hostgroup-vocmssdt</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmssdt/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmssdt/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmssdt/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmssdt/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmssdt/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmssdt/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsweb'>it-puppet-hostgroup-vocmsweb</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsweb/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsweb/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsweb/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsweb/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsweb/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsweb/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voilcdirac'>it-puppet-hostgroup-voilcdirac</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voilcdirac/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voilcdirac/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voilcdirac/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voilcdirac/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voilcdirac/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voilcdirac/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-volhcb'>it-puppet-hostgroup-volhcb</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-volhcb/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-volhcb/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-volhcb/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-volhcb/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-volhcb/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-volhcb/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vopanda'>it-puppet-hostgroup-vopanda</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vopanda/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vopanda/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vopanda/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vopanda/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vopanda/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vopanda/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-wifi_service'>it-puppet-hostgroup-wifi_service</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-wifi_service/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-wifi_service/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-wifi_service/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-wifi_service/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-wifi_service/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-wifi_service/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-wlcghammercloud'>it-puppet-hostgroup-wlcghammercloud</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-wlcghammercloud/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-wlcghammercloud/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-wlcghammercloud/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-wlcghammercloud/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-wlcghammercloud/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-wlcghammercloud/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-acronclient'>it-puppet-module-acronclient</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-acronclient/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-acronclient/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-acronclient/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-acronclient/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-acronclient/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-acronclient/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-afs'>it-puppet-module-afs</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-afs/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-afs/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-afs/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-afs/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-afs/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-afs/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-aitools'>it-puppet-module-aitools</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-aitools/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-aitools/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-aitools/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-aitools/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-aitools/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-aitools/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-apache'>it-puppet-module-apache</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-apache/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-apache/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-apache/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-apache/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-apache/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-apache/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-apiato'>it-puppet-module-apiato</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-apiato/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-apiato/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-apiato/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-apiato/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-apiato/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-apiato/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-archive'>it-puppet-module-archive</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-archive/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-archive/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-archive/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-archive/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-archive/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-archive/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-argus'>it-puppet-module-argus</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-argus/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-argus/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-argus/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-argus/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-argus/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-argus/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-asterisk'>it-puppet-module-asterisk</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-asterisk/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-asterisk/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-asterisk/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-asterisk/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-asterisk/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-asterisk/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-auditd'>it-puppet-module-auditd</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-auditd/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-auditd/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-auditd/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-auditd/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-auditd/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-auditd/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeas'>it-puppet-module-augeas</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeas/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeas/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeas/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeas/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeas/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeas/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders'>it-puppet-module-augeasproviders</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_apache'>it-puppet-module-augeasproviders_apache</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_apache/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_apache/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_apache/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_apache/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_apache/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_apache/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_base'>it-puppet-module-augeasproviders_base</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_base/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_base/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_base/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_base/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_base/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_base/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_core'>it-puppet-module-augeasproviders_core</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_core/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_core/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_core/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_core/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_core/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_core/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_grub'>it-puppet-module-augeasproviders_grub</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_grub/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_grub/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_grub/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_grub/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_grub/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_grub/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_mounttab'>it-puppet-module-augeasproviders_mounttab</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_mounttab/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_mounttab/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_mounttab/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_mounttab/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_mounttab/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_mounttab/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_nagios'>it-puppet-module-augeasproviders_nagios</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_nagios/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_nagios/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_nagios/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_nagios/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_nagios/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_nagios/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_pam'>it-puppet-module-augeasproviders_pam</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_pam/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_pam/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_pam/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_pam/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_pam/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_pam/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_postgresql'>it-puppet-module-augeasproviders_postgresql</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_postgresql/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_postgresql/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_postgresql/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_postgresql/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_postgresql/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_postgresql/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_puppet'>it-puppet-module-augeasproviders_puppet</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_puppet/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_puppet/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_puppet/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_puppet/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_puppet/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_puppet/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_shellvar'>it-puppet-module-augeasproviders_shellvar</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_shellvar/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_shellvar/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_shellvar/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_shellvar/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_shellvar/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_shellvar/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_ssh'>it-puppet-module-augeasproviders_ssh</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_ssh/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_ssh/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_ssh/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_ssh/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_ssh/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_ssh/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_sysctl'>it-puppet-module-augeasproviders_sysctl</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_sysctl/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_sysctl/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_sysctl/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_sysctl/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_sysctl/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_sysctl/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_syslog'>it-puppet-module-augeasproviders_syslog</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_syslog/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_syslog/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_syslog/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_syslog/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_syslog/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-augeasproviders_syslog/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-auks'>it-puppet-module-auks</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-auks/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-auks/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-auks/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-auks/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-auks/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-auks/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-authwrapper'>it-puppet-module-authwrapper</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-authwrapper/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-authwrapper/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-authwrapper/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-authwrapper/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-authwrapper/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-authwrapper/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-awstats'>it-puppet-module-awstats</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-awstats/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-awstats/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-awstats/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-awstats/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-awstats/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-awstats/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bagplus'>it-puppet-module-bagplus</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bagplus/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-bagplus/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bagplus/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-bagplus/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bagplus/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-bagplus/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-base'>it-puppet-module-base</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-base/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-base/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-base/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-base/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-base/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-base/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-batch_support'>it-puppet-module-batch_support</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-batch_support/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-batch_support/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-batch_support/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-batch_support/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-batch_support/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-batch_support/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bdii'>it-puppet-module-bdii</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bdii/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-bdii/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bdii/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-bdii/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bdii/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-bdii/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-beats'>it-puppet-module-beats</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-beats/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-beats/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-beats/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-beats/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-beats/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-beats/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-boinc_client'>it-puppet-module-boinc_client</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-boinc_client/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-boinc_client/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-boinc_client/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-boinc_client/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-boinc_client/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-boinc_client/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bolt'>it-puppet-module-bolt</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bolt/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-bolt/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bolt/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-bolt/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bolt/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-bolt/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bro'>it-puppet-module-bro</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bro/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-bro/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bro/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-bro/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bro/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-bro/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cadvisor'>it-puppet-module-cadvisor</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cadvisor/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cadvisor/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cadvisor/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cadvisor/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cadvisor/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cadvisor/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-castor'>it-puppet-module-castor</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-castor/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-castor/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-castor/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-castor/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-castor/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-castor/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ccitools'>it-puppet-module-ccitools</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ccitools/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ccitools/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ccitools/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ccitools/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ccitools/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ccitools/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ceph'>it-puppet-module-ceph</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ceph/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ceph/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ceph/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ceph/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ceph/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ceph/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cephfs'>it-puppet-module-cephfs</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cephfs/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cephfs/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cephfs/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cephfs/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cephfs/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cephfs/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cern_gitlab'>it-puppet-module-cern_gitlab</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cern_gitlab/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cern_gitlab/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cern_gitlab/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cern_gitlab/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cern_gitlab/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cern_gitlab/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cern_physics_storage_clients'>it-puppet-module-cern_physics_storage_clients</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cern_physics_storage_clients/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cern_physics_storage_clients/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cern_physics_storage_clients/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cern_physics_storage_clients/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cern_physics_storage_clients/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cern_physics_storage_clients/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerncollectd'>it-puppet-module-cerncollectd</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerncollectd/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cerncollectd/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerncollectd/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cerncollectd/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerncollectd/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cerncollectd/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerncollectd_contrib'>it-puppet-module-cerncollectd_contrib</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerncollectd_contrib/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cerncollectd_contrib/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerncollectd_contrib/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cerncollectd_contrib/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerncollectd_contrib/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cerncollectd_contrib/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerncondor'>it-puppet-module-cerncondor</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerncondor/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cerncondor/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerncondor/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cerncondor/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerncondor/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cerncondor/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernfw'>it-puppet-module-cernfw</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernfw/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernfw/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernfw/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernfw/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernfw/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernfw/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernipv6'>it-puppet-module-cernipv6</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernipv6/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernipv6/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernipv6/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernipv6/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernipv6/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernipv6/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernlib'>it-puppet-module-cernlib</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernlib/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernlib/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernlib/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernlib/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernlib/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernlib/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernmco'>it-puppet-module-cernmco</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernmco/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernmco/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernmco/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernmco/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernmco/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernmco/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernmegabus'>it-puppet-module-cernmegabus</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernmegabus/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernmegabus/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernmegabus/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernmegabus/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernmegabus/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernmegabus/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernnag'>it-puppet-module-cernnag</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernnag/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernnag/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernnag/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernnag/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernnag/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernnag/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernnetservices'>it-puppet-module-cernnetservices</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernnetservices/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernnetservices/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernnetservices/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernnetservices/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernnetservices/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernnetservices/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernoracle'>it-puppet-module-cernoracle</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernoracle/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernoracle/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernoracle/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernoracle/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernoracle/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernoracle/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernpuppet'>it-puppet-module-cernpuppet</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernpuppet/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernpuppet/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernpuppet/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernpuppet/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernpuppet/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernpuppet/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernrundeck'>it-puppet-module-cernrundeck</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernrundeck/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernrundeck/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernrundeck/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernrundeck/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernrundeck/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernrundeck/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernsso_apache'>it-puppet-module-cernsso_apache</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernsso_apache/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernsso_apache/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernsso_apache/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernsso_apache/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernsso_apache/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernsso_apache/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerntime'>it-puppet-module-cerntime</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerntime/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cerntime/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerntime/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cerntime/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerntime/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cerntime/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-certmgr'>it-puppet-module-certmgr</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-certmgr/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-certmgr/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-certmgr/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-certmgr/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-certmgr/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-certmgr/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cgs'>it-puppet-module-cgs</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cgs/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cgs/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cgs/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cgs/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cgs/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cgs/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-chrony'>it-puppet-module-chrony</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-chrony/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-chrony/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-chrony/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-chrony/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-chrony/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-chrony/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ci_images'>it-puppet-module-ci_images</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ci_images/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ci_images/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ci_images/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ci_images/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ci_images/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ci_images/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cloud_common'>it-puppet-module-cloud_common</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cloud_common/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cloud_common/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cloud_common/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cloud_common/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cloud_common/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cloud_common/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cloud_monitoring'>it-puppet-module-cloud_monitoring</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cloud_monitoring/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cloud_monitoring/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cloud_monitoring/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cloud_monitoring/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cloud_monitoring/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cloud_monitoring/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-collectd'>it-puppet-module-collectd</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-collectd/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-collectd/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-collectd/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-collectd/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-collectd/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-collectd/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-collectd_cadvisor'>it-puppet-module-collectd_cadvisor</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-collectd_cadvisor/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-collectd_cadvisor/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-collectd_cadvisor/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-collectd_cadvisor/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-collectd_cadvisor/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-collectd_cadvisor/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-concat'>it-puppet-module-concat</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-concat/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-concat/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-concat/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-concat/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-concat/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-concat/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-conda'>it-puppet-module-conda</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-conda/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-conda/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-conda/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-conda/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-conda/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-conda/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-consul'>it-puppet-module-consul</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-consul/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-consul/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-consul/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-consul/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-consul/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-consul/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cortex'>it-puppet-module-cortex</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cortex/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cortex/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cortex/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cortex/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cortex/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cortex/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cowrie'>it-puppet-module-cowrie</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cowrie/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cowrie/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cowrie/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cowrie/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cowrie/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cowrie/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-creamce'>it-puppet-module-creamce</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-creamce/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-creamce/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-creamce/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-creamce/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-creamce/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-creamce/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cron'>it-puppet-module-cron</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cron/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cron/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cron/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cron/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cron/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cron/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cta'>it-puppet-module-cta</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cta/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cta/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cta/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cta/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cta/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cta/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cuda'>it-puppet-module-cuda</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cuda/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cuda/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cuda/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cuda/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cuda/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cuda/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-custom'>it-puppet-module-custom</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-custom/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-custom/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-custom/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-custom/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-custom/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-custom/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cvmfs'>it-puppet-module-cvmfs</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cvmfs/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cvmfs/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cvmfs/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cvmfs/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cvmfs/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cvmfs/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cvmfssrv'>it-puppet-module-cvmfssrv</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cvmfssrv/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cvmfssrv/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cvmfssrv/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cvmfssrv/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cvmfssrv/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cvmfssrv/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dam'>it-puppet-module-dam</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dam/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dam/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dam/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dam/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dam/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dam/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-datacat'>it-puppet-module-datacat</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-datacat/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-datacat/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-datacat/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-datacat/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-datacat/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-datacat/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbcoreconfig'>it-puppet-module-dbcoreconfig</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbcoreconfig/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbcoreconfig/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbcoreconfig/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbcoreconfig/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbcoreconfig/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbcoreconfig/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbhwbunchconfig'>it-puppet-module-dbhwbunchconfig</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbhwbunchconfig/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbhwbunchconfig/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbhwbunchconfig/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbhwbunchconfig/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbhwbunchconfig/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbhwbunchconfig/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbmon'>it-puppet-module-dbmon</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbmon/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbmon/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbmon/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbmon/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbmon/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbmon/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbnetworkconfig'>it-puppet-module-dbnetworkconfig</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbnetworkconfig/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbnetworkconfig/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbnetworkconfig/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbnetworkconfig/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbnetworkconfig/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbnetworkconfig/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbod'>it-puppet-module-dbod</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbod/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbod/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbod/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbod/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbod/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbod/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dirac'>it-puppet-module-dirac</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dirac/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dirac/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dirac/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dirac/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dirac/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dirac/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-distcccern'>it-puppet-module-distcccern</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-distcccern/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-distcccern/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-distcccern/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-distcccern/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-distcccern/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-distcccern/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dmlite'>it-puppet-module-dmlite</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dmlite/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dmlite/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dmlite/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dmlite/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dmlite/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dmlite/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dnsquery'>it-puppet-module-dnsquery</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dnsquery/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dnsquery/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dnsquery/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dnsquery/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dnsquery/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dnsquery/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-docker'>it-puppet-module-docker</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-docker/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-docker/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-docker/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-docker/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-docker/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-docker/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dpm'>it-puppet-module-dpm</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dpm/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dpm/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dpm/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dpm/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dpm/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dpm/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dsc'>it-puppet-module-dsc</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dsc/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dsc/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dsc/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dsc/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dsc/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dsc/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-earlyoom'>it-puppet-module-earlyoom</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-earlyoom/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-earlyoom/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-earlyoom/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-earlyoom/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-earlyoom/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-earlyoom/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-elasticsearch'>it-puppet-module-elasticsearch</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-elasticsearch/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-elasticsearch/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-elasticsearch/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-elasticsearch/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-elasticsearch/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-elasticsearch/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-eos'>it-puppet-module-eos</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-eos/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-eos/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-eos/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-eos/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-eos/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-eos/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-eosclient'>it-puppet-module-eosclient</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-eosclient/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-eosclient/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-eosclient/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-eosclient/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-eosclient/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-eosclient/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-eosserver'>it-puppet-module-eosserver</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-eosserver/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-eosserver/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-eosserver/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-eosserver/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-eosserver/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-eosserver/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ermis'>it-puppet-module-ermis</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ermis/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ermis/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ermis/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ermis/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ermis/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ermis/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-extlib'>it-puppet-module-extlib</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-extlib/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-extlib/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-extlib/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-extlib/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-extlib/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-extlib/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-fail2ban'>it-puppet-module-fail2ban</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-fail2ban/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-fail2ban/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-fail2ban/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-fail2ban/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-fail2ban/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-fail2ban/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-fetchcrl'>it-puppet-module-fetchcrl</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-fetchcrl/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-fetchcrl/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-fetchcrl/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-fetchcrl/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-fetchcrl/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-fetchcrl/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-firewall'>it-puppet-module-firewall</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-firewall/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-firewall/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-firewall/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-firewall/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-firewall/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-firewall/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-flume'>it-puppet-module-flume</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-flume/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-flume/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-flume/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-flume/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-flume/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-flume/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-foreman'>it-puppet-module-foreman</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-foreman/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-foreman/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-foreman/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-foreman/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-foreman/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-foreman/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-foreman_proxy'>it-puppet-module-foreman_proxy</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-foreman_proxy/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-foreman_proxy/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-foreman_proxy/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-foreman_proxy/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-foreman_proxy/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-foreman_proxy/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-fts'>it-puppet-module-fts</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-fts/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-fts/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-fts/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-fts/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-fts/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-fts/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ganesha'>it-puppet-module-ganesha</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ganesha/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ganesha/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ganesha/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ganesha/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ganesha/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ganesha/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gfal2'>it-puppet-module-gfal2</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gfal2/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gfal2/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gfal2/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gfal2/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gfal2/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gfal2/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gitlab_ci_runner'>it-puppet-module-gitlab_ci_runner</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gitlab_ci_runner/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gitlab_ci_runner/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gitlab_ci_runner/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gitlab_ci_runner/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gitlab_ci_runner/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gitlab_ci_runner/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-glexecwn'>it-puppet-module-glexecwn</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-glexecwn/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-glexecwn/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-glexecwn/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-glexecwn/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-glexecwn/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-glexecwn/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gni'>it-puppet-module-gni</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gni/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gni/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gni/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gni/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gni/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gni/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gnidbdconsumer'>it-puppet-module-gnidbdconsumer</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gnidbdconsumer/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gnidbdconsumer/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gnidbdconsumer/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gnidbdconsumer/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gnidbdconsumer/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gnidbdconsumer/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gniemailconsumer'>it-puppet-module-gniemailconsumer</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gniemailconsumer/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gniemailconsumer/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gniemailconsumer/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gniemailconsumer/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gniemailconsumer/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gniemailconsumer/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gninocontact'>it-puppet-module-gninocontact</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gninocontact/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gninocontact/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gninocontact/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gninocontact/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gninocontact/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gninocontact/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gnisnowconsumer'>it-puppet-module-gnisnowconsumer</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gnisnowconsumer/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gnisnowconsumer/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gnisnowconsumer/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gnisnowconsumer/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gnisnowconsumer/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gnisnowconsumer/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-graphite'>it-puppet-module-graphite</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-graphite/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-graphite/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-graphite/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-graphite/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-graphite/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-graphite/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gridftp'>it-puppet-module-gridftp</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gridftp/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gridftp/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gridftp/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gridftp/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gridftp/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gridftp/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-grubby'>it-puppet-module-grubby</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-grubby/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-grubby/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-grubby/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-grubby/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-grubby/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-grubby/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hadoop'>it-puppet-module-hadoop</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hadoop/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hadoop/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hadoop/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hadoop/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hadoop/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hadoop/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hadoop_server'>it-puppet-module-hadoop_server</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hadoop_server/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hadoop_server/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hadoop_server/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hadoop_server/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hadoop_server/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hadoop_server/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-haproxy'>it-puppet-module-haproxy</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-haproxy/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-haproxy/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-haproxy/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-haproxy/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-haproxy/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-haproxy/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hardware'>it-puppet-module-hardware</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hardware/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hardware/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hardware/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hardware/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hardware/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hardware/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hardwareparams'>it-puppet-module-hardwareparams</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hardwareparams/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hardwareparams/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hardwareparams/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hardwareparams/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hardwareparams/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hardwareparams/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-homer'>it-puppet-module-homer</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-homer/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-homer/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-homer/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-homer/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-homer/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-homer/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hraccess'>it-puppet-module-hraccess</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hraccess/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hraccess/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hraccess/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hraccess/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hraccess/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hraccess/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-htcondor'>it-puppet-module-htcondor</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-htcondor/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-htcondor/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-htcondor/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-htcondor/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-htcondor/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-htcondor/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-htcondor_ce'>it-puppet-module-htcondor_ce</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-htcondor_ce/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-htcondor_ce/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-htcondor_ce/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-htcondor_ce/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-htcondor_ce/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-htcondor_ce/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-icinga2'>it-puppet-module-icinga2</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-icinga2/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-icinga2/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-icinga2/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-icinga2/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-icinga2/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-icinga2/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-inifile'>it-puppet-module-inifile</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-inifile/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-inifile/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-inifile/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-inifile/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-inifile/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-inifile/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-invenio'>it-puppet-module-invenio</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-invenio/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-invenio/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-invenio/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-invenio/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-invenio/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-invenio/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ipa'>it-puppet-module-ipa</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ipa/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ipa/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ipa/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ipa/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ipa/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ipa/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ipmi'>it-puppet-module-ipmi</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ipmi/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ipmi/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ipmi/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ipmi/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ipmi/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ipmi/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ipset'>it-puppet-module-ipset</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ipset/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ipset/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ipset/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ipset/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ipset/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ipset/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-java'>it-puppet-module-java</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-java/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-java/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-java/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-java/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-java/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-java/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-java_ks'>it-puppet-module-java_ks</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-java_ks/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-java_ks/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-java_ks/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-java_ks/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-java_ks/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-java_ks/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-jens'>it-puppet-module-jens</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-jens/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-jens/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-jens/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-jens/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-jens/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-jens/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-jira'>it-puppet-module-jira</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-jira/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-jira/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-jira/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-jira/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-jira/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-jira/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-jolokia'>it-puppet-module-jolokia</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-jolokia/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-jolokia/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-jolokia/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-jolokia/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-jolokia/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-jolokia/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kafka'>it-puppet-module-kafka</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kafka/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kafka/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kafka/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kafka/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kafka/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kafka/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kamailio'>it-puppet-module-kamailio</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kamailio/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kamailio/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kamailio/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kamailio/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kamailio/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kamailio/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kdump'>it-puppet-module-kdump</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kdump/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kdump/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kdump/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kdump/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kdump/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kdump/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kerberos'>it-puppet-module-kerberos</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kerberos/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kerberos/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kerberos/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kerberos/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kerberos/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kerberos/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-keytab'>it-puppet-module-keytab</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-keytab/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-keytab/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-keytab/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-keytab/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-keytab/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-keytab/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kibana4'>it-puppet-module-kibana4</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kibana4/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kibana4/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kibana4/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kibana4/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kibana4/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kibana4/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kmod'>it-puppet-module-kmod</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kmod/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kmod/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kmod/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kmod/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kmod/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kmod/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-koji'>it-puppet-module-koji</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-koji/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-koji/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-koji/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-koji/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-koji/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-koji/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kubernetes'>it-puppet-module-kubernetes</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kubernetes/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kubernetes/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kubernetes/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kubernetes/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kubernetes/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kubernetes/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-landb'>it-puppet-module-landb</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-landb/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-landb/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-landb/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-landb/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-landb/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-landb/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lbclient'>it-puppet-module-lbclient</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lbclient/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lbclient/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lbclient/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lbclient/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lbclient/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lbclient/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lbd'>it-puppet-module-lbd</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lbd/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lbd/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lbd/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lbd/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lbd/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lbd/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lcgdm'>it-puppet-module-lcgdm</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lcgdm/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lcgdm/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lcgdm/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lcgdm/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lcgdm/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lcgdm/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lemon'>it-puppet-module-lemon</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lemon/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lemon/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lemon/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lemon/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lemon/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lemon/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lemonfwd'>it-puppet-module-lemonfwd</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lemonfwd/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lemonfwd/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lemonfwd/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lemonfwd/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lemonfwd/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lemonfwd/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-letsencrypt'>it-puppet-module-letsencrypt</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-letsencrypt/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-letsencrypt/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-letsencrypt/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-letsencrypt/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-letsencrypt/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-letsencrypt/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-licmon'>it-puppet-module-licmon</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-licmon/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-licmon/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-licmon/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-licmon/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-licmon/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-licmon/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-limits'>it-puppet-module-limits</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-limits/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-limits/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-limits/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-limits/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-limits/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-limits/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-local_group_policy'>it-puppet-module-local_group_policy</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-local_group_policy/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-local_group_policy/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-local_group_policy/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-local_group_policy/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-local_group_policy/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-local_group_policy/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-logrotate'>it-puppet-module-logrotate</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-logrotate/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-logrotate/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-logrotate/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-logrotate/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-logrotate/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-logrotate/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-logstash'>it-puppet-module-logstash</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-logstash/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-logstash/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-logstash/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-logstash/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-logstash/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-logstash/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lsf'>it-puppet-module-lsf</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lsf/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lsf/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lsf/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lsf/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lsf/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lsf/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lvm'>it-puppet-module-lvm</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lvm/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lvm/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lvm/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lvm/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lvm/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lvm/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-memcached'>it-puppet-module-memcached</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-memcached/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-memcached/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-memcached/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-memcached/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-memcached/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-memcached/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-metricmgr'>it-puppet-module-metricmgr</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-metricmgr/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-metricmgr/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-metricmgr/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-metricmgr/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-metricmgr/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-metricmgr/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-misp'>it-puppet-module-misp</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-misp/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-misp/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-misp/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-misp/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-misp/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-misp/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mlocate'>it-puppet-module-mlocate</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mlocate/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-mlocate/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mlocate/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-mlocate/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mlocate/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-mlocate/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mongodb'>it-puppet-module-mongodb</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mongodb/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-mongodb/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mongodb/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-mongodb/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mongodb/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-mongodb/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-motd'>it-puppet-module-motd</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-motd/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-motd/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-motd/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-motd/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-motd/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-motd/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mpi'>it-puppet-module-mpi</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mpi/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-mpi/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mpi/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-mpi/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mpi/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-mpi/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-multifactor'>it-puppet-module-multifactor</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-multifactor/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-multifactor/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-multifactor/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-multifactor/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-multifactor/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-multifactor/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-myproxy'>it-puppet-module-myproxy</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-myproxy/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-myproxy/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-myproxy/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-myproxy/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-myproxy/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-myproxy/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mysql'>it-puppet-module-mysql</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mysql/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-mysql/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mysql/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-mysql/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mysql/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-mysql/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nagios'>it-puppet-module-nagios</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nagios/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nagios/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nagios/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nagios/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nagios/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nagios/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-network'>it-puppet-module-network</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-network/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-network/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-network/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-network/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-network/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-network/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nfs'>it-puppet-module-nfs</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nfs/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nfs/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nfs/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nfs/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nfs/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nfs/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nftables'>it-puppet-module-nftables</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nftables/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nftables/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nftables/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nftables/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nftables/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nftables/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ngauth'>it-puppet-module-ngauth</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ngauth/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ngauth/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ngauth/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ngauth/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ngauth/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ngauth/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nginx'>it-puppet-module-nginx</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nginx/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nginx/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nginx/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nginx/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nginx/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nginx/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nile'>it-puppet-module-nile</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nile/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nile/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nile/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nile/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nile/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nile/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nomad'>it-puppet-module-nomad</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nomad/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nomad/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nomad/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nomad/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nomad/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nomad/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nova'>it-puppet-module-nova</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nova/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nova/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nova/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nova/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nova/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nova/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nscd'>it-puppet-module-nscd</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nscd/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nscd/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nscd/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nscd/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nscd/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nscd/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ntp'>it-puppet-module-ntp</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ntp/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ntp/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ntp/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ntp/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ntp/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ntp/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ocsagent'>it-puppet-module-ocsagent</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ocsagent/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ocsagent/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ocsagent/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ocsagent/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ocsagent/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ocsagent/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oom_kill'>it-puppet-module-oom_kill</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oom_kill/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-oom_kill/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oom_kill/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-oom_kill/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oom_kill/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-oom_kill/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oozie'>it-puppet-module-oozie</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oozie/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-oozie/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oozie/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-oozie/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oozie/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-oozie/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-openstack_clients'>it-puppet-module-openstack_clients</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-openstack_clients/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-openstack_clients/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-openstack_clients/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-openstack_clients/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-openstack_clients/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-openstack_clients/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oracle_rdbms_12'>it-puppet-module-oracle_rdbms_12</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oracle_rdbms_12/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-oracle_rdbms_12/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oracle_rdbms_12/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-oracle_rdbms_12/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oracle_rdbms_12/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-oracle_rdbms_12/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-osrepos'>it-puppet-module-osrepos</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-osrepos/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-osrepos/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-osrepos/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-osrepos/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-osrepos/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-osrepos/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-osrepos_helper'>it-puppet-module-osrepos_helper</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-osrepos_helper/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-osrepos_helper/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-osrepos_helper/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-osrepos_helper/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-osrepos_helper/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-osrepos_helper/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-pacemaker'>it-puppet-module-pacemaker</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-pacemaker/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-pacemaker/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-pacemaker/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-pacemaker/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-pacemaker/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-pacemaker/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-patch'>it-puppet-module-patch</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-patch/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-patch/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-patch/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-patch/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-patch/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-patch/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-php'>it-puppet-module-php</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-php/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-php/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-php/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-php/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-php/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-php/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-podman'>it-puppet-module-podman</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-podman/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-podman/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-podman/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-podman/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-podman/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-podman/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-postfix'>it-puppet-module-postfix</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-postfix/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-postfix/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-postfix/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-postfix/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-postfix/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-postfix/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-postgresql'>it-puppet-module-postgresql</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-postgresql/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-postgresql/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-postgresql/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-postgresql/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-postgresql/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-postgresql/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-postgrest'>it-puppet-module-postgrest</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-postgrest/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-postgrest/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-postgrest/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-postgrest/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-postgrest/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-postgrest/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-powerdns'>it-puppet-module-powerdns</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-powerdns/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-powerdns/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-powerdns/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-powerdns/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-powerdns/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-powerdns/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-psacct'>it-puppet-module-psacct</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-psacct/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-psacct/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-psacct/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-psacct/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-psacct/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-psacct/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-puppetdb'>it-puppet-module-puppetdb</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-puppetdb/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-puppetdb/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-puppetdb/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-puppetdb/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-puppetdb/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-puppetdb/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-puppetdbquery'>it-puppet-module-puppetdbquery</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-puppetdbquery/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-puppetdbquery/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-puppetdbquery/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-puppetdbquery/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-puppetdbquery/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-puppetdbquery/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-puppetserver'>it-puppet-module-puppetserver</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-puppetserver/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-puppetserver/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-puppetserver/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-puppetserver/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-puppetserver/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-puppetserver/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-purge'>it-puppet-module-purge</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-purge/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-purge/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-purge/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-purge/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-purge/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-purge/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-python'>it-puppet-module-python</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-python/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-python/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-python/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-python/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-python/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-python/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rally'>it-puppet-module-rally</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rally/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-rally/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rally/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-rally/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rally/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-rally/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rally_tester'>it-puppet-module-rally_tester</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rally_tester/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-rally_tester/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rally_tester/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-rally_tester/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rally_tester/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-rally_tester/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-reboot'>it-puppet-module-reboot</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-reboot/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-reboot/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-reboot/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-reboot/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-reboot/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-reboot/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-redis'>it-puppet-module-redis</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-redis/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-redis/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-redis/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-redis/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-redis/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-redis/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-riak'>it-puppet-module-riak</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-riak/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-riak/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-riak/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-riak/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-riak/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-riak/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rsync'>it-puppet-module-rsync</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rsync/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-rsync/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rsync/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-rsync/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rsync/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-rsync/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rsyslog'>it-puppet-module-rsyslog</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rsyslog/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-rsyslog/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rsyslog/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-rsyslog/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rsyslog/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-rsyslog/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rundeck'>it-puppet-module-rundeck</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rundeck/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-rundeck/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rundeck/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-rundeck/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rundeck/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-rundeck/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-security'>it-puppet-module-security</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-security/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-security/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-security/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-security/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-security/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-security/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-selinux'>it-puppet-module-selinux</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-selinux/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-selinux/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-selinux/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-selinux/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-selinux/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-selinux/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sendmail'>it-puppet-module-sendmail</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sendmail/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sendmail/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sendmail/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sendmail/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sendmail/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sendmail/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-serf'>it-puppet-module-serf</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-serf/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-serf/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-serf/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-serf/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-serf/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-serf/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-shibboleth'>it-puppet-module-shibboleth</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-shibboleth/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-shibboleth/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-shibboleth/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-shibboleth/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-shibboleth/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-shibboleth/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-singularity'>it-puppet-module-singularity</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-singularity/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-singularity/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-singularity/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-singularity/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-singularity/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-singularity/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-slurm'>it-puppet-module-slurm</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-slurm/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-slurm/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-slurm/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-slurm/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-slurm/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-slurm/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-snmp'>it-puppet-module-snmp</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-snmp/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-snmp/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-snmp/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-snmp/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-snmp/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-snmp/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-splunk'>it-puppet-module-splunk</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-splunk/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-splunk/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-splunk/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-splunk/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-splunk/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-splunk/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-splunkforwarder'>it-puppet-module-splunkforwarder</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-splunkforwarder/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-splunkforwarder/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-splunkforwarder/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-splunkforwarder/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-splunkforwarder/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-splunkforwarder/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-squid'>it-puppet-module-squid</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-squid/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-squid/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-squid/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-squid/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-squid/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-squid/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ssh'>it-puppet-module-ssh</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ssh/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ssh/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ssh/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ssh/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ssh/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ssh/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sslcertificate'>it-puppet-module-sslcertificate</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sslcertificate/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sslcertificate/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sslcertificate/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sslcertificate/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sslcertificate/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sslcertificate/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sssd'>it-puppet-module-sssd</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sssd/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sssd/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sssd/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sssd/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sssd/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sssd/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-st2'>it-puppet-module-st2</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-st2/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-st2/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-st2/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-st2/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-st2/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-st2/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-stdlib'>it-puppet-module-stdlib</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-stdlib/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-stdlib/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-stdlib/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-stdlib/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-stdlib/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-stdlib/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-storage_monitoring'>it-puppet-module-storage_monitoring</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-storage_monitoring/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-storage_monitoring/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-storage_monitoring/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-storage_monitoring/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-storage_monitoring/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-storage_monitoring/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sts'>it-puppet-module-sts</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sts/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sts/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sts/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sts/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sts/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sts/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sudo'>it-puppet-module-sudo</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sudo/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sudo/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sudo/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sudo/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sudo/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sudo/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-supervisor'>it-puppet-module-supervisor</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-supervisor/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-supervisor/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-supervisor/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-supervisor/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-supervisor/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-supervisor/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-swap_file'>it-puppet-module-swap_file</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-swap_file/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-swap_file/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-swap_file/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-swap_file/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-swap_file/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-swap_file/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sysctl'>it-puppet-module-sysctl</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sysctl/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sysctl/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sysctl/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sysctl/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sysctl/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sysctl/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sysfs'>it-puppet-module-sysfs</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sysfs/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sysfs/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sysfs/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sysfs/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sysfs/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sysfs/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-systemd'>it-puppet-module-systemd</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-systemd/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-systemd/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-systemd/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-systemd/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-systemd/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-systemd/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-teigi'>it-puppet-module-teigi</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-teigi/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-teigi/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-teigi/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-teigi/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-teigi/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-teigi/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-telegraf'>it-puppet-module-telegraf</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-telegraf/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-telegraf/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-telegraf/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-telegraf/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-telegraf/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-telegraf/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-terraform'>it-puppet-module-terraform</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-terraform/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-terraform/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-terraform/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-terraform/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-terraform/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-terraform/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tftp'>it-puppet-module-tftp</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tftp/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tftp/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tftp/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tftp/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tftp/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tftp/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-thehive'>it-puppet-module-thehive</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-thehive/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-thehive/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-thehive/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-thehive/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-thehive/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-thehive/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tomcat'>it-puppet-module-tomcat</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tomcat/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tomcat/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tomcat/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tomcat/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tomcat/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tomcat/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-traefik'>it-puppet-module-traefik</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-traefik/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-traefik/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-traefik/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-traefik/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-traefik/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-traefik/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-training'>it-puppet-module-training</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-training/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-training/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-training/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-training/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-training/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-training/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-translate'>it-puppet-module-translate</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-translate/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-translate/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-translate/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-translate/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-translate/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-translate/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tsmclient'>it-puppet-module-tsmclient</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tsmclient/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tsmclient/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tsmclient/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tsmclient/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tsmclient/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tsmclient/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tsmserver'>it-puppet-module-tsmserver</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tsmserver/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tsmserver/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tsmserver/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tsmserver/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tsmserver/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tsmserver/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tuned'>it-puppet-module-tuned</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tuned/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tuned/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tuned/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tuned/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tuned/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tuned/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-umd'>it-puppet-module-umd</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-umd/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-umd/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-umd/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-umd/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-umd/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-umd/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vcsrepo'>it-puppet-module-vcsrepo</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vcsrepo/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vcsrepo/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vcsrepo/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vcsrepo/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vcsrepo/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vcsrepo/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vegas'>it-puppet-module-vegas</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vegas/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vegas/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vegas/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vegas/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vegas/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vegas/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-voatlastools'>it-puppet-module-voatlastools</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-voatlastools/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-voatlastools/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-voatlastools/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-voatlastools/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-voatlastools/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-voatlastools/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vocmsgwmsfrontend'>it-puppet-module-vocmsgwmsfrontend</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vocmsgwmsfrontend/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vocmsgwmsfrontend/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vocmsgwmsfrontend/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vocmsgwmsfrontend/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vocmsgwmsfrontend/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vocmsgwmsfrontend/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor'>it-puppet-module-vocmshtcondor</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-voms'>it-puppet-module-voms</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-voms/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-voms/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-voms/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-voms/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-voms/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-voms/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vosupport'>it-puppet-module-vosupport</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vosupport/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vosupport/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vosupport/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vosupport/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vosupport/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vosupport/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-watchdog'>it-puppet-module-watchdog</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-watchdog/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-watchdog/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-watchdog/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-watchdog/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-watchdog/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-watchdog/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-websvn'>it-puppet-module-websvn</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-websvn/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-websvn/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-websvn/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-websvn/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-websvn/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-websvn/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-windows_disable_ipv6'>it-puppet-module-windows_disable_ipv6</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-windows_disable_ipv6/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-windows_disable_ipv6/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-windows_disable_ipv6/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-windows_disable_ipv6/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-windows_disable_ipv6/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-windows_disable_ipv6/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-windowsfeature'>it-puppet-module-windowsfeature</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-windowsfeature/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-windowsfeature/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-windowsfeature/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-windowsfeature/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-windowsfeature/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-windowsfeature/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-wsus_client'>it-puppet-module-wsus_client</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-wsus_client/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-wsus_client/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-wsus_client/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-wsus_client/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-wsus_client/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-wsus_client/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-xinetd'>it-puppet-module-xinetd</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-xinetd/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-xinetd/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-xinetd/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-xinetd/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-xinetd/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-xinetd/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-xrootd'>it-puppet-module-xrootd</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-xrootd/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-xrootd/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-xrootd/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-xrootd/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-xrootd/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-xrootd/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-yum'>it-puppet-module-yum</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-yum/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-yum/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-yum/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-yum/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-yum/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-yum/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-zeppelin'>it-puppet-module-zeppelin</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-zeppelin/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-zeppelin/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-zeppelin/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-zeppelin/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-zeppelin/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-zeppelin/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-zfs'>it-puppet-module-zfs</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-zfs/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-zfs/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-zfs/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-zfs/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-zfs/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-zfs/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-zookeeper'>it-puppet-module-zookeeper</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-zookeeper/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-zookeeper/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-zookeeper/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-zookeeper/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-zookeeper/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-zookeeper/badges/modulesync/pipeline.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-site'>it-puppet-site</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-site/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-site/badges/master/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-site/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-site/badges/qa/pipeline.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-site/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-site/badges/modulesync/pipeline.svg'></a></td>
 </table>


